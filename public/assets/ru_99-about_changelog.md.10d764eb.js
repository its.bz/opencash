import{_ as e,c as a,o as t,d as n}from"./app.a68eb506.js";const u='{"title":"\u0418\u0441\u0442\u043E\u0440\u0438\u044F \u0438\u0437\u043C\u0435\u043D\u0435\u043D\u0438\u0439","description":"","frontmatter":{},"headers":[{"level":2,"title":"\u041A\u043E\u043D\u0442\u0430\u043A\u0442\u044B","slug":"\u043A\u043E\u043D\u0442\u0430\u043A\u0442\u044B"},{"level":2,"title":"\u041B\u0438\u0446\u0435\u043D\u0437\u0438\u044F","slug":"\u043B\u0438\u0446\u0435\u043D\u0437\u0438\u044F"}],"relativePath":"ru/99-about/changelog.md"}',i={},r=n(`<h1 id="\u0438\u0441\u0442\u043E\u0440\u0438\u044F-\u0438\u0437\u043C\u0435\u043D\u0435\u043D\u0438\u0439" tabindex="-1">\u0418\u0441\u0442\u043E\u0440\u0438\u044F \u0438\u0437\u043C\u0435\u043D\u0435\u043D\u0438\u0439 <a class="header-anchor" href="#\u0438\u0441\u0442\u043E\u0440\u0438\u044F-\u0438\u0437\u043C\u0435\u043D\u0435\u043D\u0438\u0439" aria-hidden="true">#</a></h1><div class="language-"><pre><code>v1.0.2 @ 27 January 2022  
- Enhance changelog generation and language switching for Docs

v1.0.1 @ 27 January 2022  
- Add documentation  
- Merge tag &#39;1.0.0&#39; into dev

1.0.0 @ 26 January 2022  
- Initial commit  
- Add README.md with descriptions for all of modules  
- Initial commit  
- Initial commit  
- Initial commit  
- Initial commit

</code></pre></div><h2 id="\u043A\u043E\u043D\u0442\u0430\u043A\u0442\u044B" tabindex="-1">\u041A\u043E\u043D\u0442\u0430\u043A\u0442\u044B <a class="header-anchor" href="#\u043A\u043E\u043D\u0442\u0430\u043A\u0442\u044B" aria-hidden="true">#</a></h2><p>\u0421\u0432\u044F\u0436\u0438\u0442\u0435\u0441\u044C \u0441 \u043D\u0430\u043C\u0438: <a href="mailto:dev@its.bz">dev@its.bz</a><br> \u041D\u0430\u0448 \u0441\u0430\u0439\u0442: <a href="https://its.bz" target="_blank" rel="noopener noreferrer">its.bz</a></p><h2 id="\u043B\u0438\u0446\u0435\u043D\u0437\u0438\u044F" tabindex="-1">\u041B\u0438\u0446\u0435\u043D\u0437\u0438\u044F <a class="header-anchor" href="#\u043B\u0438\u0446\u0435\u043D\u0437\u0438\u044F" aria-hidden="true">#</a></h2><p>MIT</p>`,6),o=[r];function d(c,s,l,h,_,m){return t(),a("div",null,o)}var g=e(i,[["render",d]]);export{u as __pageData,g as default};
