import{_ as e,c as a,o as n,d as t}from"./app.a68eb506.js";const u='{"title":"Changelog","description":"","frontmatter":{},"headers":[{"level":2,"title":"Credits","slug":"credits"},{"level":2,"title":"License","slug":"license"}],"relativePath":"en/99-about/changelog.md"}',i={},r=t(`<h1 id="changelog" tabindex="-1">Changelog <a class="header-anchor" href="#changelog" aria-hidden="true">#</a></h1><div class="language-"><pre><code>v1.0.2 @ 27 January 2022  
- Enhance changelog generation and language switching for Docs

v1.0.1 @ 27 January 2022  
- Add documentation  
- Merge tag &#39;1.0.0&#39; into dev

1.0.0 @ 26 January 2022  
- Initial commit  
- Add README.md with descriptions for all of modules  
- Initial commit  
- Initial commit  
- Initial commit  
- Initial commit

</code></pre></div><h2 id="credits" tabindex="-1">Credits <a class="header-anchor" href="#credits" aria-hidden="true">#</a></h2><p>Contact us: <a href="mailto:dev@its.bz">dev@its.bz</a><br> Our site: <a href="https://its.bz" target="_blank" rel="noopener noreferrer">its.bz</a></p><h2 id="license" tabindex="-1">License <a class="header-anchor" href="#license" aria-hidden="true">#</a></h2><p>MIT</p>`,6),o=[r];function c(s,d,l,h,_,g){return n(),a("div",null,o)}var p=e(i,[["render",c]]);export{u as __pageData,p as default};
