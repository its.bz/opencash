# История изменений

```
{{#each releases}}{{title}} @ {{niceDate}}{{#commits}}  
  - {{subject}}{{/commits}}

{{/each}}
```

## Контакты

Свяжитесь с нами: [dev@its.bz](mailto:dev@its.bz)\
Наш сайт: [its.bz](https://its.bz)

## Лицензия

MIT
