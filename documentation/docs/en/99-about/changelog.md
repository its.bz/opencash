# Changelog

```
v1.0.2 @ 27 January 2022  
- Enhance changelog generation and language switching for Docs

v1.0.1 @ 27 January 2022  
- Add documentation  
- Merge tag '1.0.0' into dev

1.0.0 @ 26 January 2022  
- Initial commit  
- Add README.md with descriptions for all of modules  
- Initial commit  
- Initial commit  
- Initial commit  
- Initial commit

```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

MIT
