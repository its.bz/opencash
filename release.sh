#!/bi/bash

if yarn git-branch-is main; then
  (
    yarn version --patch --no-git-tag-version
    git add package.json
    ver=$(yarn -s print-version)

    git commit -m "$ver"
    git tag "$ver"
    git push origin main --follow-tags
  )
fi
