# OpenCash
Open source app for small and middle business.

[![timesheet](https://wakatime.com/badge/user/b63eae7a-adaa-4419-a51e-0f59a1a4a46f/project/bb82c2bc-783c-4987-8c0e-50235e2e1559.svg)](https://wakatime.com/projects/haka.its.bz)
[![pipeline status](https://gitlab.com/itsbz/opencash/badges/main/pipeline.svg)](https://gitlab.com/its.bz/haka/-/commits/main)
[![coverage report](https://gitlab.com/itsbz/opencash/badges/main/coverage.svg)](https://gitlab.com/its.bz/haka/-/commits/main)

## What includes?
+ Main API server
+ Manager
+ Cash
+ VDU+EQ
+ Menuboard
+ Music streaming app
+ Client's app

## Project setup
```
yarn install
```

Each module has its own README.md with instructions


## Development

Use `git flow` as develop workflow
```shell
git flow feature start <here-name-of-a-future>
...do something great and commit changes
git flow feature finish <here-name-of-a-future>
```
Then you can send PR. 

### Documentation
Main documentation store accessed here:
- [English](https://itsbz.gitlab.io/opencash/en/)
- [Russian](https://itsbz.gitlab.io/opencash/ru/)


### Contributing

ITS welcomes contributions from anyone and everyone. 
Please see our [guidelines for contributing](./CONTRIBUTING.md).

### License

Copyright 2021-2022 ITS.bz

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

A copy of the license is available in the repository's [LICENSE](./LICENSE.md) file.
