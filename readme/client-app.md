# APP
Client's app. It is PWA for clients. They can register and use cashback, order dishes and receive news over PUSH.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
```
