import { config } from '@/config'
import { DataSource, DataSourceOptions } from 'typeorm'

export const appDataSource = new DataSource({
	type: config.database.type,
	host: config.database.host,
	port: config.database.port,
	username: config.database.user,
	password: config.database.password,
	database: config.database.name,
	autoLoadEntities: true,
	entities: [__dirname + '/src/**/*.entity{.ts,.js}'],
	synchronize: config.env === 'development',
	migrationsRun: config.env === 'production',
	migrations: ['src/database/migrations/**/*.ts'],
	cli: {
		entitiesDir: 'src/modules/',
		migrationsDir: 'src/database/migrations/',
		seedsDir: 'src/database/seeds/',
	},
} as DataSourceOptions)
