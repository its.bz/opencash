export const i18n = (
	key: keyof typeof words,
	params: unknown = null,
	lang = 'ru',
) => {
	if (!words[key]) {
		console.warn(`There are no translation for key "${key}"`)
		if (params)
			Object.keys(params).map(
				(p) => (key = key.toString().replace(`{${p}}`, params[p])),
			)
		return key
	}
	if (!words[key][lang]) {
		console.warn(`There are no translation for key "${key}" and lang "${lang}"`)
		if (params)
			Object.keys(params).map((p) =>
				key.toString().replace(`{${p}}`, params[p]),
			)
		return key
	}

	if (typeof words[key][lang] === 'function') {
		return words[key][lang](params)
	}

	return words[key][lang]
}

const words: { [key: string]: { ru?: any; es?: any } } = {
	'Hello!': {
		ru: 'Привет!',
		es: 'Ola!',
	},
	'Hello, $user!': {
		ru: ({ user }) => `Привет, ${user}!`,
		es: ({ user }) => `Ola, ${user}!`,
	},
	'Wrong phone number or SMS code': {
		ru: 'Неверный телефон или код из SMS',
	},
	'Your code is:': { ru: 'Ваш код:' },
	'Wrong validation code!': {
		ru: 'Неверный код подтверждения!',
	},
	'No user found for email: {email}': {
		ru: ({ email }) => `Не найден пользователь с email: ${email}`,
	},
}
