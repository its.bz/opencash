import {
	BaseEntity,
	Column,
	CreateDateColumn,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm'
import { Field, Int, ObjectType } from '@nestjs/graphql'

@ObjectType({ isAbstract: true })
export abstract class BaseModel extends BaseEntity {
	@PrimaryGeneratedColumn()
	@Field(() => Int || String)
	id: number | string

	@CreateDateColumn({
		type: 'timestamp',
		default: () => 'CURRENT_TIMESTAMP',
	})
	@Field()
	createdAt?: Date

	createdBy?: string | number

	@UpdateDateColumn({
		type: 'timestamp',
		default: () => 'CURRENT_TIMESTAMP',
		onUpdate: 'CURRENT_TIMESTAMP',
	})
	@Field()
	updatedAt?: Date

	updatedBy?: string | number

	@Column({ type: 'boolean', default: false })
	@Field()
	isDeleted?: boolean
}
