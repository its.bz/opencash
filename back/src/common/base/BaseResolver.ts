import { BaseModel } from './BaseModel'
import { FindOneOptions, ObjectLiteral } from 'typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql'
import { AuthUser } from '@/common/decorators/auth-user.decorator'
import { Type } from '@nestjs/common'
import { BaseService } from '@/common/base/BaseService'

export function BaseResolver<
	Entity extends BaseModel & ObjectLiteral,
	CreateDto extends Partial<Entity>,
	UpdateDto extends Partial<Entity>,
>(
	classRef: Type<Entity>,
	CreateClassRefInput: Type<CreateDto>,
	UpdateClassRefInput: Type<UpdateDto>,
): any {
	@Resolver({ isAbstract: true })
	abstract class BaseResolverHost {
		public service: BaseService<Entity, CreateDto, UpdateDto>
		protected constructor(service: BaseService<Entity, CreateDto, UpdateDto>) {
			this.service = service
		}

		@Mutation(() => [classRef])
		create(
			@AuthUser() user: UserEntity,
			@Args('createInput', { type: () => CreateClassRefInput })
			createInput: CreateDto,
		) {
			return this.service.create(createInput, user)
		}

		// @ts-ignore
		@Query(() => [this.objectType])
		findAll() {
			return this.service.findAll()
		}

		// @ts-ignore
		@Query(() => this.objectType)
		// @ts-ignore
		findOne(
			@Args('filter', { type: () => this.findInput })
			filter: FindOneOptions<Entity>,
		) {
			return this.service.findOne(filter)
		}

		@Mutation(() => this.objectType)
		updateItem(
			@AuthUser() user: UserEntity,
			@Args('updateItemInput', { type: () => UpdateClassRefInput })
			updateItemInput: UpdateDto,
		) {
			return this.service.update(
				// @ts-ignore
				{ where: { id: updateItemInput.id } },
				updateItemInput,
				user,
			)
		}

		@Mutation(() => this.objectType)
		removeItem(
			@AuthUser() user: UserEntity,
			@Args('id', { type: () => Int }) id: number,
		) {
			// @ts-ignore
			return this.service.remove({ where: { id } }, user)
		}
	}
	return BaseResolverHost
}
