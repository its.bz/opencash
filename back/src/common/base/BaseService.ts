import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import {
	FindManyOptions,
	FindOneOptions,
	ObjectLiteral,
	Repository,
	TypeORMError,
} from 'typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { BaseModel } from '@/common/base/BaseModel'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'

@Injectable()
export abstract class BaseService<
	Entity extends BaseModel & ObjectLiteral,
	CreateDto extends Partial<Entity>,
	UpdateDto extends Partial<Entity>,
> {
	protected repo: Repository<Entity>

	async create(
		createDto: CreateDto,
		user: UserEntity,
		findConditionsAndOptions?: FindOneOptions<Entity>,
	): Promise<Entity> {
		try {
			const record = await this.repo.insert({
				...createDto,
				createdBy: user.id,
			} as QueryDeepPartialEntity<Entity>)
			return this.findOne({
				where: { id: record.raw[0].id },
				...findConditionsAndOptions,
			})
		} catch (e) {
			throw new TypeORMError(e)
		}
	}

	async findAll(findConditionsAndOptions: FindManyOptions<Entity> = {}) {
		return await this.repo.find(findConditionsAndOptions)
	}

	async findOne(findConditionsAndOptions?: FindOneOptions<Entity>) {
		return await this.repo.findOne(findConditionsAndOptions)
	}

	async update(
		findConditionsAndOptions: FindOneOptions<Entity>,
		updateDto: UpdateDto,
		user: UserEntity,
	) {
		const record = await this.repo.findOne(findConditionsAndOptions)
		if (!record)
			throw new HttpException('Record not found', HttpStatus.NOT_FOUND)
		Object.assign(record, { ...updateDto, updatedBy: user.id })

		try {
			await record.save()
			return record
		} catch (e) {
			throw new TypeORMError(e)
		}
	}

	async remove(
		findConditionsAndOptions: FindOneOptions<Entity> = {},
		user: UserEntity,
	) {
		try {
			const record = await this.repo.findOne(findConditionsAndOptions)
			record.isDeleted = !record.isDeleted
			record.updatedBy = user.id
			await record.save()
			return record
		} catch (e) {
			throw new TypeORMError(e)
		}
	}
}
