import { ExecutionContext, Injectable } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { GqlExecutionContext } from '@nestjs/graphql'

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
	constructor() {
		super()
	}

	canActivate(context: ExecutionContext) {
		const req =
			context.switchToHttp().getRequest() ||
			GqlExecutionContext.create(context).getContext().req
		const JwtToken = req.headers['authorization']
		if (JwtToken) return super.canActivate(context)
		return false
	}

	getRequest(context: ExecutionContext) {
		const ctx = GqlExecutionContext.create(context)
		return context.switchToHttp().getRequest() || ctx.getContext().req
	}
}
