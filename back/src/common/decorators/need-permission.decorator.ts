import { SetMetadata } from '@nestjs/common'
import { PermissionsEnum } from '@/common/constants/enums'

export const PERMISSION_KEY = 'permission'
export const NeedPermission = (permission: PermissionsEnum) =>
	SetMetadata(PERMISSION_KEY, permission)
