import { ApiProperty } from '@nestjs/swagger'
import { UserEntity } from '@/modules/users/enities/user.entity'

export class MessageCreateDto {
	@ApiProperty({ example: { id: 1 } })
	from: UserEntity

	@ApiProperty({ example: { id: 1 } })
	to: UserEntity

	@ApiProperty({ example: 'Тут сообщение' })
	message: string
}
