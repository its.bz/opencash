import { BaseModel } from '@/common/base/BaseModel'
import { Column, Entity, ManyToOne } from 'typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { IsNotEmpty } from 'class-validator'
import { Field, ObjectType } from '@nestjs/graphql'
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator'

@Entity('messages')
@ObjectType()
export class MessageEntity extends BaseModel {
	@ManyToOne(() => UserEntity, (user) => user.messagesFrom)
	@Field(() => UserEntity)
	@ApiModelProperty({ type: () => UserEntity })
	from: UserEntity

	@ManyToOne(() => UserEntity, (user) => user.messagesTo)
	@Field(() => UserEntity)
	@ApiModelProperty({ type: () => UserEntity })
	to: UserEntity

	@Column()
	@IsNotEmpty()
	@Field()
	@ApiModelProperty()
	message: string
}
