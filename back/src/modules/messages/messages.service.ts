import { Injectable } from '@nestjs/common'
import { MessageCreateDto } from './dto/message-create.dto'
import { MessageUpdateDto } from './dto/message-update.dto'
import { BaseService } from '@/common/base/BaseService'
import { MessageEntity } from '@/modules/messages/entities/message.entity'

@Injectable()
export class MessagesService extends BaseService<
	MessageEntity,
	MessageCreateDto,
	MessageUpdateDto
> {}
