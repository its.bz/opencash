import {
	Body,
	Controller,
	Delete,
	Get,
	HttpException,
	HttpStatus,
	Param,
	Patch,
	Post,
	Req,
	UseGuards,
} from '@nestjs/common'
import { MessagesService } from './messages.service'
import { MessageCreateDto } from './dto/message-create.dto'
import { MessageUpdateDto } from './dto/message-update.dto'
import { RequestWithUser } from '@/modules/users/dto/user.dto'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { PermissionsEnum } from '@/common/constants/enums'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'

@ApiTags('Messages')
@Controller('messages')
export class MessagesController {
	constructor(private readonly messageService: MessagesService) {}

	@Post()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	create(
		@Req() { user }: RequestWithUser,
		@Body() createDto: MessageCreateDto,
	) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers)) {
			return this.messageService.create(createDto, user)
		} else {
			if (createDto.from.id === user.id)
				return this.messageService.create(createDto, user)
			else
				throw new HttpException(
					'Вам нельзя создавать сообщения не от себя!',
					HttpStatus.UNAUTHORIZED,
				)
		}
	}

	@Get()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	findAll(@Req() { user }: RequestWithUser) {
		return user.getPermission(PermissionsEnum.AllowCreateUsers)
			? this.messageService.findAll()
			: this.messageService.findAll({
					where: [{ from: { id: user.id } }, { to: { id: user.id } }],
			  })
	}

	@Get(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	findOne(@Req() { user }: RequestWithUser, @Param('id') id: number) {
		return this.messageService.findOne({ where: { id } }).then((r) => {
			if (
				user.getPermission(PermissionsEnum.AllowCreateUsers) ||
				r.from.id === user.id ||
				r.to.id === user.id
			)
				return r
			throw new HttpException(
				'Нельзя читать чужие сообщения!',
				HttpStatus.UNAUTHORIZED,
			)
		})
	}

	@Patch(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	async update(
		@Req() { user }: RequestWithUser,
		@Param('id') id: number,
		@Body() updateDto: MessageUpdateDto,
	) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers)) {
			return this.messageService.update({ where: { id } }, updateDto, user)
		} else {
			const record = await this.messageService.findOne({ where: { id } })
			if (updateDto.from.id === user.id && record.from.id === user.id)
				return this.messageService.update({ where: { id } }, updateDto, user)
			else
				throw new HttpException(
					'Вам нельзя изменять сообщения не от себя!',
					HttpStatus.UNAUTHORIZED,
				)
		}
	}

	@Delete(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	remove(@Req() { user }: RequestWithUser, @Param('id') id: number) {
		return this.messageService.remove({ where: { id } }, user)
	}
}
