import { MailerService } from '@nestjs-modules/mailer'
import { Injectable } from '@nestjs/common'
import { config } from '@/config'

@Injectable()
export class MailService {
	constructor(private mailerService: MailerService) {}

	async sendActivationEmail(email: string, code: string, ip: string) {
		const url = `//${config.app.publicUrl}/${config.app.routePrefix}/auth/activate-email/${email}/by-code/${code}`

		await this.mailerService.sendMail({
			to: email,
			// from: '"Support Team" <support@example.com>', // override default from
			subject: 'Welcome to the App! Activate your account',
			template: './en/activation', // `.hbs` extension is appended automatically
			context: {
				email,
				url,
				code,
			},
		})
	}

	async sendNewPasswordEmail(email: string, password: string) {
		const url = `//${config.app.publicUrl}/auth/login`
		await this.mailerService.sendMail({
			to: email,
			// from: '"Support Team" <support@example.com>', // override default from
			subject: 'App: Your new password',
			template: './en/new-password', // `.hbs` extension is appended automatically
			context: {
				email,
				password,
			},
		})
	}
}
