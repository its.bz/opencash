import { MailerModule } from '@nestjs-modules/mailer'
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter'
import { Module } from '@nestjs/common'
import { MailService } from './mail.service'
import { join } from 'path'
import { config } from '@/config'

@Module({
	imports: [
		MailerModule.forRoot({
			transport: {
				host: config.mail.host,
				secure: config.mail.secure,
				requireTLS: config.mail.useTLS,
				debug: config.mail.debug,
				port: config.mail.port,
				auth: {
					user: config.mail.auth.user,
					pass: config.mail.auth.pass,
				},
			},
			defaults: {
				from: config.mail.from,
			},
			template: {
				dir: join(__dirname, 'templates'),
				adapter: new HandlebarsAdapter(),
				options: {
					strict: true,
				},
			},
		}),
	],
	providers: [MailService],
	exports: [MailService],
})
export class MailModule {}
