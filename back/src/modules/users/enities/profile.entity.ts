import { Column, Entity, OneToOne } from 'typeorm'
import { Field, ObjectType } from '@nestjs/graphql'
import { BaseModel } from '@/common/base/BaseModel'
import { UserEntity } from '@/modules/users/enities/user.entity'

@Entity('profiles')
@ObjectType()
export class ProfileEntity extends BaseModel {
	@OneToOne(() => UserEntity, (user) => user.profile)
	@Field(() => UserEntity)
	user: UserEntity

	@Column()
	@Field()
	profession: string
}
