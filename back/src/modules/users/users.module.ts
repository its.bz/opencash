import { Module } from '@nestjs/common'
import { UsersService } from './users.service'
import { UsersController } from './users.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { UsersResolver } from '@/modules/users/users.resolver'

@Module({
	imports: [TypeOrmModule.forFeature([UserEntity])],
	controllers: [UsersController],
	providers: [UsersService, UsersResolver],
	exports: [UsersService],
})
export class UsersModule {}
