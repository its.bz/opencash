import { PartialType } from '@nestjs/mapped-types'
import { IsObject } from 'class-validator'
import { UserCreateDto } from './user-create.dto'
import { ApiProperty } from '@nestjs/swagger'
import { ProfileEntity } from '@/modules/users/enities/profile.entity'
import { InputType } from '@nestjs/graphql'

@InputType()
export class UserUpdateDto extends PartialType(UserCreateDto) {
	@IsObject()
	@ApiProperty({ type: ProfileEntity })
	profile?: ProfileEntity
}
