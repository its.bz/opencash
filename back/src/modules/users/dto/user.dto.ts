import { Request } from 'express'
import { UserEntity } from '@/modules/users/enities/user.entity'

export type UserDto = Partial<UserEntity>

export interface RequestWithUser extends Request {
	user: UserEntity
}
