import { Field, InputType } from '@nestjs/graphql'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { PartialType } from '@nestjs/mapped-types'

@InputType()
export class UserCreateDto extends PartialType(UserEntity) {
	@Field()
	name?: string
}
