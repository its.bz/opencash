import { ArgsType, Field, registerEnumType } from '@nestjs/graphql'
import { PermissionsEnum } from '@/common/constants/enums'

@ArgsType()
export class CanIArgs {
	@Field(() => PermissionsEnum, {
		defaultValue: PermissionsEnum.AllowToLogin,
	})
	permission: PermissionsEnum
}

registerEnumType(PermissionsEnum, {
	name: 'PermissionsEnum',
	description: 'Possible list of permissions.',
})
