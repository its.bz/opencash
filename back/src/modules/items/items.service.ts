import { Injectable } from '@nestjs/common'
import { ItemCreateInput } from './dto/item-create.input'
import { ItemUpdateInput } from './dto/item-update.input'
import { BaseService } from '@/common/base/BaseService'
import { ItemEntity } from '@/modules/items/entities/item.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

@Injectable()
export class ItemsService extends BaseService<
	ItemEntity,
	ItemCreateInput,
	ItemUpdateInput
> {
	constructor(
		@InjectRepository(ItemEntity) protected repo: Repository<ItemEntity>,
	) {
		super()
	}
}
