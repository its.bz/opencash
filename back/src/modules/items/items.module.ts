import { Module } from '@nestjs/common'
import { ItemsService } from './items.service'
import { ItemsResolver } from './items.resolver'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ItemEntity } from '@/modules/items/entities/item.entity'
import { ItemsController } from './items.controller'

@Module({
	imports: [TypeOrmModule.forFeature([ItemEntity])],
	providers: [ItemsResolver, ItemsService],
	controllers: [ItemsController],
})
export class ItemsModule {}
