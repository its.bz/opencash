import { ItemCreateInput } from './item-create.input'
import { InputType, Field, Int, PartialType } from '@nestjs/graphql'

@InputType()
export class ItemUpdateInput extends PartialType(ItemCreateInput) {
	@Field(() => Int)
	id: number

	@Field(() => String)
	name: string
}
