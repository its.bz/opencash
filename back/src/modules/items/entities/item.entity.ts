import { ObjectType, Field } from '@nestjs/graphql'
import { BaseModel } from '@/common/base/BaseModel'
import { Entity } from 'typeorm'

@ObjectType()
@Entity('items')
export class ItemEntity extends BaseModel {
	@Field(() => String)
	name: string
}
