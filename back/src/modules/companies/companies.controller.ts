import {
	Body,
	Controller,
	Delete,
	Get,
	HttpException,
	HttpStatus,
	Param,
	Patch,
	Post,
	Req,
	UseGuards,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { CompaniesService } from './companies.service'
import { CompanyCreateDto } from './dto/company-create.dto'
import { CompanyUpdateDto } from './dto/company-update.dto'
import { RequestWithUser } from '@/modules/users/dto/user.dto'
import { PermissionsEnum } from '@/common/constants/enums'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'

@ApiTags('Companies')
@Controller('companies')
export class CompaniesController {
	constructor(private readonly companiesService: CompaniesService) {}

	@Post()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	create(
		@Req() { user }: RequestWithUser,
		@Body() createCompanyDto: CompanyCreateDto,
	) {
		return this.companiesService.create(createCompanyDto, user)
	}

	@Get()
	findAll() {
		return this.companiesService.findAll()
	}

	@Get(':id')
	async findOne(@Param('id') id: number) {
		return await this.companiesService.findOne({ where: { id } })
	}

	@Patch(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	async update(
		@Req() { user }: RequestWithUser,
		@Param('id') id: number,
		@Body() updateCompanyDto: CompanyUpdateDto,
	) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers)) {
			return this.companiesService.update(
				{ where: { id } },
				updateCompanyDto,
				user,
			)
		} else {
			const company = await this.companiesService.findOne({ where: { id } })
			if (company.owner.id === user.id)
				return this.companiesService.update(
					{ where: { id } },
					updateCompanyDto,
					user,
				)
			else
				throw new HttpException(
					'Вам нельзя изменять не свою компанию!',
					HttpStatus.UNAUTHORIZED,
				)
		}
	}

	@Delete(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	async remove(@Req() { user }: RequestWithUser, @Param('id') id: number) {
		if (user.getPermission(PermissionsEnum.AllowCreateUsers)) {
			return this.companiesService.remove({ where: { id } }, user)
		} else {
			const company = await this.companiesService.findOne({ where: { id } })
			if (company.owner.id === user.id)
				return this.companiesService.remove({ where: { id } }, user)
			else
				return new HttpException(
					'Вам нельзя удалять не свою компанию!',
					HttpStatus.UNAUTHORIZED,
				)
		}
	}
}
