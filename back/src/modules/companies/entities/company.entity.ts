import { Column, Entity, ManyToOne } from 'typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { BaseModel } from '@/common/base/BaseModel'
import { Field, ObjectType } from '@nestjs/graphql'

@Entity('companies')
@ObjectType()
export class CompanyEntity extends BaseModel {
	@Column()
	@Field()
	name: string

	@Column({ nullable: true })
	@Field()
	about?: string

	@Column()
	@Field()
	inn?: string

	@ManyToOne(() => UserEntity, (user) => user.companies, { eager: true })
	@Field(() => UserEntity)
	owner: UserEntity
}
