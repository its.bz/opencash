import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { AuthUser } from '@/common/decorators/auth-user.decorator'
import { CompanyEntity } from '@/modules/companies/entities/company.entity'
import { CompaniesService } from '@/modules/companies/companies.service'
import { CompanyCreateDto } from '@/modules/companies/dto/company-create.dto'
import { CompanyUpdateDto } from '@/modules/companies/dto/company-update.dto'

@Resolver(() => CompanyEntity)
export class CompaniesResolver {
	constructor(private companiesService: CompaniesService) {}

	@Mutation(() => CompanyEntity)
	create(
		@AuthUser() user: UserEntity,
		@Args('companyCreateDto') companyCreateDto: CompanyCreateDto,
	) {
		return this.companiesService.create(companyCreateDto, user)
	}

	@Query(() => [CompanyEntity], { name: 'companies', nullable: true })
	findAll() {
		return this.companiesService.findAll()
	}

	@Query(() => CompanyEntity, { name: 'company' })
	findOne(@Args('id', { type: () => Int }) id: number) {
		return this.companiesService.findOne({ where: { id } })
	}

	@Mutation(() => CompanyEntity)
	update(
		@AuthUser() user: UserEntity,
		@Args('companyUpdateDto') companyUpdateDto: CompanyUpdateDto,
	) {
		return this.companiesService.update(
			{ where: { id: CompanyUpdateDto.id } },
			companyUpdateDto,
			user,
		)
	}

	@Mutation(() => CompanyEntity)
	remove(
		@AuthUser() user: UserEntity,
		@Args('id', { type: () => Int }) id: number,
	) {
		return this.companiesService.remove({ where: { id } }, user)
	}
}
