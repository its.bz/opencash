import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { CompanyCreateDto } from './dto/company-create.dto'
import { CompanyUpdateDto } from './dto/company-update.dto'
import { BaseService } from '@/common/base/BaseService'
import { CompanyEntity } from '@/modules/companies/entities/company.entity'
import { Repository } from 'typeorm'

@Injectable()
export class CompaniesService extends BaseService<
	CompanyEntity,
	CompanyCreateDto,
	CompanyUpdateDto
> {
	constructor(
		@InjectRepository(CompanyEntity) protected repo: Repository<CompanyEntity>,
	) {
		super()
	}
}
