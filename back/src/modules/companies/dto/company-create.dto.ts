import { UserEntity } from '@/modules/users/enities/user.entity'
import { ApiProperty } from '@nestjs/swagger'
import { Field, InputType } from '@nestjs/graphql'
import { UserCreateDto } from '@/modules/users/dto/user-create.dto'

@InputType()
export class CompanyCreateDto {
	@ApiProperty({ example: 'ИП Васюков А.Г.' })
	@Field()
	name: string

	@ApiProperty({ example: '3123318751' })
	@Field()
	inn: string

	@ApiProperty({ type: UserEntity })
	@Field(() => UserCreateDto)
	owner: UserEntity
}
