import { ApiProperty, PartialType } from '@nestjs/swagger'
import { CompanyCreateDto } from './company-create.dto'
import { Field, InputType } from '@nestjs/graphql'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { UserCreateDto } from '@/modules/users/dto/user-create.dto'

@InputType()
export class CompanyUpdateDto extends PartialType(CompanyCreateDto) {
	@ApiProperty({ example: '1', required: true })
	@Field()
	static id: number

	@ApiProperty({ example: 'ИП Васюков А.Г.', required: false })
	@Field()
	name: string

	@ApiProperty({ example: '3123318751', required: false })
	@Field()
	inn: string

	@ApiProperty({ type: UserEntity, required: false })
	@Field(() => UserCreateDto)
	owner: UserEntity
}
