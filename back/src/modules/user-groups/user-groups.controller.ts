import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
	UseGuards,
} from '@nestjs/common'
import { UserGroupsService } from './user-groups.service'
import { CreateUserGroupDto } from './dto/create-user-group.dto'
import { UpdateUserGroupDto } from './dto/update-user-group.dto'
import { AuthUser } from '@/common/decorators/auth-user.decorator'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { NeedPermission } from '@/common/decorators/need-permission.decorator'
import { PermissionsEnum } from '@/common/constants/enums'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'
import { PermissionGuard } from '@/common/guards/pernission-guard.service'

@Controller('user-groups')
@ApiTags('User groups')
export class UserGroupsController {
	constructor(private readonly userGroupsService: UserGroupsService) {}

	@Get()
	@NeedPermission(PermissionsEnum.AllowCreateUsers)
	@UseGuards(PermissionGuard)
	findAll() {
		return this.userGroupsService.findAll()
	}

	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.userGroupsService.findOne({ where: { id } })
	}

	@Post()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	@NeedPermission(PermissionsEnum.AllowCreateUsers) //TODO: Create permission
	create(
		@AuthUser() user: UserEntity,
		@Body() createUserGroupDto: CreateUserGroupDto,
	) {
		return this.userGroupsService.create(createUserGroupDto, user)
	}

	@Patch(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	update(
		@Param('id') id: string,
		@AuthUser() user: UserEntity,
		@Body() updateUserGroupDto: UpdateUserGroupDto,
	) {
		return this.userGroupsService.update(
			{ where: { id } },
			updateUserGroupDto,
			user,
		)
	}

	@Delete(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	remove(@AuthUser() user: UserEntity, @Param('id') id: string) {
		return this.userGroupsService.remove({ where: { id } }, user)
	}
}
