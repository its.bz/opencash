import { Injectable } from '@nestjs/common'
import { CreateUserGroupDto } from './dto/create-user-group.dto'
import { UpdateUserGroupDto } from './dto/update-user-group.dto'
import { BaseService } from '@/common/base/BaseService'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { UserGroupEntity } from '@/modules/user-groups/entities/user-group.entity'

@Injectable()
export class UserGroupsService extends BaseService<
	UserGroupEntity,
	CreateUserGroupDto,
	UpdateUserGroupDto
> {
	constructor(
		@InjectRepository(UserGroupEntity)
		protected repo: Repository<UserGroupEntity>,
	) {
		super()
	}
}
