import { Module } from '@nestjs/common'
import { UserGroupsService } from './user-groups.service'
import { UserGroupsController } from './user-groups.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserGroupEntity } from '@/modules/user-groups/entities/user-group.entity'

@Module({
	imports: [TypeOrmModule.forFeature([UserGroupEntity])],
	controllers: [UserGroupsController],
	providers: [UserGroupsService],
})
export class UserGroupsModule {}
