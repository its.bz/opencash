import { Field, ObjectType } from '@nestjs/graphql'
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator'
import { BaseModel } from '@/common/base/BaseModel'
import { Column, Entity, OneToMany } from 'typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import {
	Permission,
	PermissionClass,
	PermissionsEnum,
} from '@/common/constants/enums'

@Entity('user-groups')
@ObjectType()
export class UserGroupEntity extends BaseModel {
	@Field(() => String, { nullable: true })
	@ApiModelProperty()
	@Column()
	name: string

	@Column()
	@Field()
	@ApiModelProperty()
	description?: string

	@Column({ type: 'jsonb', nullable: true })
	@Field(() => PermissionClass, { nullable: true })
	@ApiModelProperty({ type: () => PermissionClass })
	permissions?: Permission

	@OneToMany(() => UserEntity, (user) => user.group)
	@Field(() => [UserEntity])
	@ApiModelProperty({ type: UserEntity })
	users: UserEntity[]
}
