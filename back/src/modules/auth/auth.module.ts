import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { AuthController } from './auth.controller'
import { UsersModule } from '@/modules/users/users.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { config } from '@/config'
import { JwtStrategy } from '@/modules/auth/strategies/jwt.stategy'
import { AuthResolver } from '@/modules/auth/auth.resolver'
import { PasswordService } from '@/modules/auth/password.service'
import { UserGroupsService } from '../user-groups/user-groups.service'
import { UserGroupEntity } from '../user-groups/entities/user-group.entity'
import { MailService } from '@/modules/mail/mail.service'
import { MailModule } from '@/modules/mail/mail.module'

@Module({
	imports: [
		TypeOrmModule.forFeature([UserEntity, UserGroupEntity]),
		UsersModule,
		MailModule,
		PassportModule.register({
			defaultStrategy: 'jwt',
			property: 'user',
			session: false,
		}),
		JwtModule.register({
			secret: config.security.jwt_secret,
			signOptions: { expiresIn: config.security.jwt_expires_in },
		}),
	],
	controllers: [AuthController],
	providers: [
		AuthService,
		JwtStrategy,
		AuthResolver,
		PasswordService,
		UserGroupsService,
		MailService,
	],
	exports: [AuthService],
})
export class AuthModule {}
