import { ArgsType, Field } from '@nestjs/graphql'
import { ApiProperty } from '@nestjs/swagger'
import { IsJWT, IsNotEmpty } from 'class-validator'
import { GraphQLJWT } from 'graphql-scalars'

@ArgsType()
export class RefreshTokenInput {
	@ApiProperty()
	@IsNotEmpty()
	@IsJWT()
	@Field(() => GraphQLJWT)
	token: string
}
