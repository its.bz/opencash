import { IsJWT } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'
import { ApiProperty } from '@nestjs/swagger'

@InputType()
export class ActivatationInput {
	@ApiProperty()
	@Field()
	@IsJWT()
	activationToken: string
}
