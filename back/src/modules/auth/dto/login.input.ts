import { IsEmail, IsMobilePhone, IsNotEmpty, MinLength } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'
import { ApiProperty } from '@nestjs/swagger'

@InputType()
export class LoginByEmailInput {
	@ApiProperty({
		examples: { admin: 'admin@example.com', user: 'user@example.com' },
	})
	@Field()
	@IsEmail()
	email: string

	@ApiProperty({
		example: 'secret42',
	})
	@Field()
	@IsNotEmpty()
	@MinLength(6)
	password?: string
}

@InputType()
export class LoginByPhoneInput {
	@ApiProperty({
		type: String,
		examples: { one: '+79107373125', two: '+77770375732' },
	})
	@IsMobilePhone()
	@Field()
	phone: string
}

@InputType()
export class ValidatePhoneInput {
	@ApiProperty({ examples: { ru: '+79107373125', kz: '+77770375732' } })
	@IsMobilePhone()
	@Field()
	phone: string

	@ApiProperty()
	@Field()
	@ApiProperty({ example: '1234' })
	code: string
}

@InputType()
export class ValidateEmailInput {
	@ApiProperty({
		examples: { admin: 'admin@example.com', user: 'user@example.com' },
	})
	@IsEmail()
	@Field()
	email: string

	@ApiProperty()
	@Field()
	@ApiProperty({ example: '1234' })
	code: string
}
