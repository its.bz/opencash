import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { ApiBearerAuth } from '@nestjs/swagger'
import { UseGuards } from '@nestjs/common'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'
import { AuthUser } from '@/common/decorators/auth-user.decorator'
import { AuthService } from '@/modules/auth/auth.service'
import { LoginByEmailInput, ValidatePhoneInput } from './dto/login.input'
import { UserWithTokens } from './models/auth.model'
import { Request } from 'express'

@Resolver(() => UserEntity)
export class AuthResolver {
	constructor(private authService: AuthService) {}

	@Query(() => UserEntity)
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	async me(@AuthUser() user: UserEntity) {
		return user
	}

	@Mutation(() => Boolean)
	loginByPhone(@Args('phone') phone: string, @Context('request') req: Request) {
		return this.authService.getValidationCodeBySms(phone, req.ip)
	}

	@Mutation(() => UserWithTokens)
	validatePhone(
		@Args('loginByPhoneInput') { phone, code }: ValidatePhoneInput,
	) {
		return this.authService.validatePhone(phone, code)
	}

	@Mutation(() => UserWithTokens)
	loginByEmail(
		@Args('loginByEmailInput') { email, password }: LoginByEmailInput,
		@Context('request') req: Request,
	) {
		email = email.toLowerCase()
		return this.authService.loginByEmail(email, password, req.ip)
	}

	@Mutation(() => UserWithTokens)
	validateEmail(@Args('email') email: string, @Args('code') code: string) {
		return { email, code }
	}
}
