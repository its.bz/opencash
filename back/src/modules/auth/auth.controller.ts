import {
	Body,
	Controller,
	Get,
	Param,
	Post,
	Req,
	UseGuards,
} from '@nestjs/common'
import { Request } from 'express'
import { AuthService } from './auth.service'
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger'
import {
	LoginByEmailInput,
	LoginByPhoneInput,
	ValidateEmailInput,
	ValidatePhoneInput,
} from './dto/login.input'
import { JwtAuthGuard } from '@/common/guards/jwt-auth-guard.service'
import { AuthUser } from '@/common/decorators/auth-user.decorator'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { UserWithTokens } from '@/modules/auth/models/auth.model'
import { MailService } from '@/modules/mail/mail.service'

@Controller('auth')
@ApiTags('Authentication')
export class AuthController {
	constructor(
		private readonly authService: AuthService,
		private readonly mailService: MailService,
	) {}

	@Get('me')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	me(@AuthUser() user: UserEntity): UserEntity {
		delete user.password
		return user
	}

	@Post('loginByPhone')
	loginByPhone(@Body() { phone }: LoginByPhoneInput, @Req() req: Request) {
		//return this.authService.getValidationCodeBySms(phone, req.ip)
		return this.authService.getValidationCodeByCall(phone, req.ip)
	}

	@Post('validatePhone')
	@ApiOkResponse({
		description: 'User object and two tokens',
		type: UserWithTokens,
	})
	validatePhone(
		@Body() { phone, code }: ValidatePhoneInput,
	): Promise<UserWithTokens> {
		return this.authService.validatePhone(phone, code)
	}

	@Post('loginByEmail')
	loginByEmail(
		@Body() { email, password }: LoginByEmailInput,
		@Req() req: Request,
	) {
		email = email.toLowerCase()
		return this.authService.loginByEmail(email, password, req.ip)
	}

	@Post('validateEmail')
	validateEmail(@Body() { email, code }: ValidateEmailInput) {
		return this.authService.validateEmail(email, code)
	}

	@Get('activate-email/:email/by-code/:code')
	validateEmailByCode(
		@Param('email') email: string,
		@Param('code') code: string,
	) {
		return this.authService.validateEmail(email, code)
	}
}
