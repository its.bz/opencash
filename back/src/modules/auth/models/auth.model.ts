import { Field, ObjectType } from '@nestjs/graphql'
import { Tokens } from './token.model'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { ApiProperty } from '@nestjs/swagger'

@ObjectType()
export class UserWithTokens {
	@Field()
	@ApiProperty()
	user: UserEntity

	@Field()
	@ApiProperty()
	tokens: Tokens
}
