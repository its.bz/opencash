import axios from 'axios'
import {
	BadRequestException,
	HttpException,
	HttpStatus,
	Injectable,
	InternalServerErrorException,
	NotFoundException,
	UnauthorizedException,
} from '@nestjs/common'
import { UserEntity } from '@/modules/users/enities/user.entity'
import { config } from '@/config'
import { UsersService } from '@/modules/users/users.service'
import { JwtService } from '@nestjs/jwt'
import { UserDto } from '@/modules/users/dto/user.dto'
import { PermissionsEnum } from '@/common/constants/enums'
import { Tokens } from '@/modules/auth/models/token.model'
import { PasswordService } from '@/modules/auth/password.service'
import { UserGroupsService } from '@/modules/user-groups/user-groups.service'
import { i18n } from '@/common/i18n'
import { UserWithTokens } from './models/auth.model'
import { MailService } from '@/modules/mail/mail.service'

@Injectable()
export class AuthService {
	constructor(
		private readonly usersService: UsersService,
		private readonly userGroupService: UserGroupsService,
		private readonly passwordService: PasswordService,
		private readonly jwtService: JwtService,
		private readonly mailService: MailService,
	) {}

	private userCodes: { [key: string]: string } = {}

	/**
	 * Returns random code (numbers for radix=10 and alphanumeric for radix=36)
	 * of given lengths
	 * @param length
	 * @param radix
	 */
	getRandomCode(length = 4, radix: 10 | 16 | 36 = 10) {
		return Math.random()
			.toString(radix)
			.slice(2, length + 2)
			.toUpperCase()
	}

	/**
	 * Sends SMS and store code into the in-memory array `this.userCodes`
	 * @param phone
	 * @param ip
	 */
	async getValidationCodeBySms(phone: string, ip: string) {
		const code = this.getRandomCode()
		const text = `${i18n('Your code is:')} ${code}`
		this.userCodes[phone] = code
		return await this.sendSms(phone, text, ip)
	}

	/**
	 * Doing call and store code into the in-memory array `this.userCodes`
	 * @param phone
	 * @param ip
	 */
	async getValidationCodeByCall(phone: string, ip: string) {
		this.userCodes[phone] = await this.doCall(phone, ip)
		return { status: 'OK' }
	}

	/**
	 * Doing phone call using external service and return last 4 digits
	 * @param phone
	 * @param ip User's unique IP for spam protect
	 */
	async doCall(phone: string, ip: string): Promise<string> {
		const params = {
			api_id: config.integrations.smsRu.token,
			phone,
			ip,
			json: 1,
			partner_id: 44454,
		}
		const { status, code } = (
			await axios.get('https://sms.ru/code/call', { params })
		).data
		if (status !== 'OK')
			throw new InternalServerErrorException(
				'SMS.ru phone calling unknown error',
			)
		return code
	}

	/**
	 * Sends message to SMS using external service
	 * @param phone
	 * @param text
	 * @param ip User's unique IP for spam protect
	 */
	async sendSms(phone: string, text: string, ip: string) {
		const params = {
			api_id: config.integrations.smsRu.token,
			to: phone,
			msg: text,
			ip,
			json: 1,
		}
		const { data } = await axios.get('https://sms.ru/sms/send', { params })
		if (data.status !== 'OK')
			throw new InternalServerErrorException(data.sms || data.status_text)
		return { status: data.status }
	}

	async validatePhone(phone: string, code: string): Promise<UserWithTokens> {
		if (code != this.userCodes[phone])
			throw new BadRequestException(i18n('Wrong validation number!'))
		delete this.userCodes[phone]

		let user = await this.usersService.findOne({
			where: { phone },
		})
		if (!user) {
			const password = this.getRandomCode(10)
			user = await this.usersService.create(
				{
					phone,
					password: await this.passwordService.hashPassword(password),
					group: await this.userGroupService.findOne({
						where: { name: 'Users' },
					}),
				},
				await this.usersService.findOne({
					where: { name: 'Root' },
				}),
			)
		}
		const tokens = await this.generateTokens(user)
		return { user, tokens }
	}

	async validateUser(payload: UserDto): Promise<UserEntity> {
		const user = await this.usersService.findOne({ where: { id: payload.id } })
		if (!user) {
			throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED)
		} else if (!user.getPermission(PermissionsEnum.AllowToLogin)) {
			throw new HttpException('Login denied', HttpStatus.FORBIDDEN)
		}
		return user
	}

	async loginByEmail(
		email: string,
		password: string,
		ip: string,
	): Promise<UserWithTokens> {
		const user = await this.usersService.findOne({
			where: { email },
		})

		if (!user) {
			this.userCodes[email] = this.getRandomCode(4)
			await this.mailService.sendActivationEmail(
				email,
				this.userCodes[email],
				ip,
			)
			throw new NotFoundException(
				i18n(`No user found for email: {email}`, { email }),
			)
		}

		const passwordValid = await this.passwordService.validatePassword(
			password,
			user.password,
		)

		if (!passwordValid) {
			throw new BadRequestException(i18n('Invalid password'))
		}

		delete user.password
		const tokens = await this.generateTokens(user)
		return { user, tokens }
	}

	async validateEmail(email: string, code: string): Promise<UserWithTokens> {
		if (code != this.userCodes[email])
			throw new BadRequestException(i18n('Wrong validation code!'))
		delete this.userCodes[email]

		let user = await this.usersService.findOne({
			where: { email },
		})
		if (!user) {
			const password = this.getRandomCode(10)
			await this.mailService.sendNewPasswordEmail(email, password)
			user = await this.usersService.create(
				{
					email,
					password: await this.passwordService.hashPassword(password),
					group: await this.userGroupService.findOne({
						where: { name: 'Users' },
					}),
				},
				await this.usersService.findOne({
					where: { name: 'Root' },
				}),
			)
		}
		const tokens = await this.generateTokens(user)
		return { user, tokens }
	}

	getUserFromToken(token: string): Promise<UserEntity> {
		const id = this.jwtService.decode(token)['id']
		return this.usersService.findOne({ where: { id } })
	}

	async generateTokens(user: UserEntity): Promise<Tokens> {
		return {
			accessToken: this.generateAccessToken(user),
			refreshToken: this.generateRefreshToken(user),
		}
	}

	private generateAccessToken(payload: UserEntity): string {
		return this.jwtService.sign({ id: payload.id })
	}

	private generateRefreshToken(payload: UserEntity): string {
		const securityConfig = config.security
		return this.jwtService.sign(
			{ id: payload.id },
			{
				secret: securityConfig.jwt_secret,
				expiresIn: securityConfig.jwt_expires_in,
			},
		)
	}

	async refreshToken(token: string) {
		try {
			const user = await this.getUserFromToken(token)
			return this.generateTokens(user)
		} catch (e) {
			throw new UnauthorizedException()
		}
	}
}
