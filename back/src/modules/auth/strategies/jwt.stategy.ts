import { config } from '@/config'
import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { AuthService } from '@/modules/auth/auth.service'
import { UserDto } from '@/modules/users/dto/user.dto'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private readonly authService: AuthService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: config.security.jwt_secret,
		})
	}

	async validate(payload: UserDto) {
		const user = await this.authService.validateUser(payload)
		if (!user) {
			throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED)
		}
		return user
	}
}
