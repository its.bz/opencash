import { Injectable } from '@nestjs/common'
import {
	PERMISSIONS_DESCRIPTIONS,
	PermissionsEnum,
} from '@/common/constants/enums'

@Injectable()
export class PermissionsService {
	findAll() {
		return PERMISSIONS_DESCRIPTIONS
	}

	findOne(id: PermissionsEnum) {
		return PERMISSIONS_DESCRIPTIONS[id]
	}
}
