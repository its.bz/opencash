import { Controller, Get, Param } from '@nestjs/common'
import { PermissionsService } from './permissions.service'
import { PermissionsEnum } from '@/common/constants/enums'
import { ApiParam, ApiTags } from '@nestjs/swagger'

@Controller('permissions')
@ApiTags('Permissions')
export class PermissionsController {
	constructor(private readonly permissionsService: PermissionsService) {}

	@Get()
	findAll() {
		return this.permissionsService.findAll()
	}

	@Get(':id')
	@ApiParam({ name: 'id', type: () => PermissionsEnum, enum: PermissionsEnum })
	findOne(@Param('id') id: PermissionsEnum) {
		return this.permissionsService.findOne(id)
	}
}
