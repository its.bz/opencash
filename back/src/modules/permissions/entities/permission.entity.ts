import { Field, ObjectType } from '@nestjs/graphql'
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator'
import { PermissionsEnum } from '@/common/constants/enums'

@ObjectType()
export class PermissionEntity {
	@Field(() => String)
	id: PermissionsEnum

	@Field()
	@ApiModelProperty()
	name: string

	@Field()
	@ApiModelProperty()
	description?: string
}
