import { Injectable } from '@nestjs/common'
import { config } from '@/config'

export interface Hello {
	name: string
	version: string
}

@Injectable()
export class AppService {
	getHello(): Hello {
		return { name: config.pkg.description, version: config.pkg.version }
	}
}
