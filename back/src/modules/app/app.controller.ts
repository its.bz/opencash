import { Controller, Get } from '@nestjs/common'
import { AppService, Hello } from './app.service'

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get('/')
	getRoot(): Hello {
		return this.appService.getHello()
	}
}
