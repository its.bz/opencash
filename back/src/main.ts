import { config } from './config'
import { NestFactory, Reflector } from '@nestjs/core'
import swaggerInit from '@/swagger'
import { AppModule } from '@/modules/app/app.module'
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common'
import {
	AnyExceptionFilter,
	HttpExceptionFilter,
} from '@/common/filters/HttpException.filter'
import { TransformInterceptor } from '@/common/interceptors/rest-response.interceptor'

async function bootstrap() {
	const app = await NestFactory.create(AppModule)
	app.setGlobalPrefix(config.app.routePrefix)
	app.useGlobalPipes(new ValidationPipe())
	app.useGlobalFilters(new HttpExceptionFilter(), new AnyExceptionFilter())
	app.useGlobalInterceptors(
		new TransformInterceptor(),
		new ClassSerializerInterceptor(app.get(Reflector)),
	)
	app.enableCors()
	swaggerInit(app)
	await app.listen(config.app.port)

	console.log(`
  ${config.pkg.name} ver.${config.pkg.version} by ${config.pkg.author}
  Started at http://127.0.0.1:${config.app.port}/${config.app.routePrefix}
  NODE_ENV=${config.env}
  `)
}

bootstrap().catch((e) => {
	throw new Error(e)
})
