// eslint-disable-next-line @typescript-eslint/no-var-requires
const tsconfig = require('./tsconfig.json')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const moduleNameMapper = require('tsconfig-paths-jest')(tsconfig)

module.exports = {
	transform: {
		'^.+\\.tsx?$': 'ts-jest',
	},
	testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
	rootDir: '.',
	collectCoverageFrom: ['./src/**/*.(t|j)s'],
	coverageDirectory: './coverage',
	testEnvironment: 'node',
	coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
	moduleNameMapper,
}
